class CharacterParams:
    INTELLIGENCE = "intelligence"
    WISDOM = "wisdom"
    CHARISMA = "charisma"
    STRENGTH = "strength"
    DEXTERITY = "dexterity"
    SPEED = "speed"
    ENDURANCE = "endurance"

    STAT_NAMES = [INTELLIGENCE,
                  WISDOM,
                  CHARISMA,
                  STRENGTH,
                  DEXTERITY,
                  SPEED,
                  ENDURANCE]

    def __init__(self, stats):
        self._stats = stats

    @staticmethod
    def is_value_valid(stat_value):
        return 0 < stat_value < 20

    @property
    def stats(self):
        return self._stats


class Hero:
    foo = "placeholder"


def print_stats(stats, remaining_points, print_instruction=False):
    for stat_name, value in stats.items():
        print(stat_name, " -> ", value)

    if print_instruction:
        print("Remaining points: ", remaining_points)
        print("To change values, please use stat_name+ or stat_name- command.")
        print("For example:")
        print(CharacterParams.CHARISMA, "+ to increase", CharacterParams.CHARISMA)
        print(CharacterParams.CHARISMA, "- to decrease", CharacterParams.CHARISMA)
        print("Only values between 0 and 20 (exclusive) are allowed.")
