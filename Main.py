from Game import Game
from Constatns import MONSTERS, LOCATIONS
from GameFactory import GameFactory
import persistence
from Hero import CharacterParams, print_stats
import random


def start_game(stat):
    print("Welcome to my game\n")

    factory = GameFactory(LOCATIONS, MONSTERS, 3)

    game_state = {"visited_levels": factory.generate_game_state(),
                  "level_index": 0, "stats": stat}

    my_game = Game()

    level = my_game.prepare_level(game_state)
    while level:
        level()
        game_state["level_index"] += 1
        level = my_game.prepare_level(game_state)
    print("Congratulation! You won the game")


def load_game():
    print("Game loaded\n")

    game_state = persistence.load_game()

    my_game = Game()

    level = my_game.prepare_level(game_state)
    while level:
        level()
        game_state["level_index"] += 1
        level = my_game.prepare_level(game_state)


def print_menu():
    print("Welcome into the game. Choose one:\n"
          "1: Start new game\n"
          "2: Load game")


def create_character():
    stats = {name: random.randrange(1, 20) for name in CharacterParams.STAT_NAMES}
    points_to_distribute = max(0, 63 - sum(stats.values()))
    operations = {"+": lambda x: x + 1, "-": lambda x: x - 1}

    def modify_stat(stat_name, operation):
        nonlocal points_to_distribute
        old_value = stats[stat_name]
        new_value = operations[operation](old_value)
        if CharacterParams.is_value_valid(new_value):
            new_points_to_distribute = points_to_distribute - operations[operation](0)
            if new_points_to_distribute < 0:
                print("You do not have enough points remaining")
                return
            stats[stat_name] = new_value
            points_to_distribute = new_points_to_distribute
        else:
            print("Stat value out of range.")

    is_finished = False

    while not is_finished:
        print_stats(stats, points_to_distribute, print_instruction=True)
        print("Please type ok to accept your character and finish.")
        action = input(">> ").strip().lower()
        if action == "ok":
            is_finished = True
            correct_input = True
        elif len(action) > 1:
            operation = action[-1]
            stat_name = action[:-1]
            if stat_name in CharacterParams.STAT_NAMES and operation in operations:
                correct_input = True
                modify_stat(stat_name, operation)
            else:
                correct_input = False
        else:
            correct_input = False

        if not correct_input:
            print("Incorrect input, please try again.")

    return CharacterParams(stats)


if __name__ == "__main__":
    action = None
    while not action == "1" and not action == "2":  # "#action != "1" or action != "2":
        print_menu()
        action = input(">> ").strip()
    if action == "1":
        stats = create_character()
        start_game(stats)
    elif action == "2":
        load_game()
