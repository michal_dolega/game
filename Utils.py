def validate_number_as_string(string_to_validate):
    return string_to_validate.isnumeric()

def validate_number(number, min, max):
    if number.isnumeric():
        number = int(number)
        return number >= min and number <= max
    return False

def validate_string_as_int_in_range(string, min, max):
    return validate_number_as_string(string) and validate_number(string, min, max)