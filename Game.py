# This file contains two main classes
# Game - Responsible for main logic, preparation of game, transition between levels and locations
# Location - Responsible for information about location, internal game and status of visit

from Challenges.TestGame import TestGame
from Challenges.GuessNumber import GuessNumber
from Challenges.TicTacToe import TicTacToe
import random
from Location import Location
from persistence import save_game
from Hero import print_stats


class Game:
    def __init__(self):
        pass

    def prepare_level(self, game_state):
        level_index = game_state["level_index"]
        if level_index in game_state["visited_levels"]:
            def play_game():
                print("Welcome. You are now in level number {}\n".format(level_index))
                while not self._is_level_finished(game_state["visited_levels"][level_index]):
                    self._visit_location(game_state)
            return play_game
        else:
            return None

    def show_remaining_challenges(self, locations_list):
        completed = 0
        uncompleted = 0
        for loc in locations_list:
            if loc.visited:
                completed += 1
            else:
                uncompleted += 1
        print("In current level you completed {} challenges. {} left".format(completed, uncompleted))

    def _is_level_finished(self, locations_list):
        for loc in locations_list:
            if loc.visited is False:
                return False
        return True

    def _visit_location(self, game_state):
        unvisited_location_list = game_state["visited_levels"][game_state["level_index"]]

        chosen_location = None
        while chosen_location is None:
            action = self._get_user_input(unvisited_location_list)
            chosen_location = self.parse_input(action, game_state)

        is_won = unvisited_location_list[chosen_location].game.play(game_state["stats"].stats)

        if is_won:
            print("Congratulation! You passed this challenge!\n")
            del (unvisited_location_list[chosen_location])

        else:
            print("You failed, but You can try again\n")

    def parse_input(self, action, game_state):
        if action.isdigit():
            chosen_location = int(action)
            is_available = 0 <= chosen_location < len(game_state["visited_levels"][game_state["level_index"]])
            return chosen_location if is_available else None
        elif action == "s":
            save_game(game_state)
            print("Game saved!")
            return None
        elif action == "p":
            print_stats(game_state["stats"].stats, None)
            return None
        else:
            return None

    def _get_user_input(self, unvisited_location_list):
        print("Choose level to play\n")
        for number, loc in enumerate(unvisited_location_list):
            print("{}: {}".format(number, loc.loc_name))
        print("-" * 10)
        print("s: Save game")
        print("p: Character stats")
        print()

        return input(">> ").strip().lower()
