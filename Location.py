class Location:

    def __init__(self, loc_name, game):
        self.loc_name = loc_name
        self.visited = False
        self.game = game 

    def __str__(self):
        result = ""
        result += "Location name: {}\n".format(self.loc_name)
        result += "Is visited: {}\n".format(self.visited)
        result += "Game class name: {}\n".format(self.game.__class__.__name__)
        return result