from math import e
from Hero import CharacterParams
import random

_BASE_HP = 100
_BASE_HEALING = 20
_BASE_ATTACK = 20
_MAX_STAT_VALUE = 19
_MIN_STAT_VALUE = 1


def _compute_stat_effectiveness(value):
    return (value - _MIN_STAT_VALUE) / (_MAX_STAT_VALUE - _MIN_STAT_VALUE)


def _compute_initial_hp(stats):
    endurance = stats[CharacterParams.ENDURANCE]
    endurance_eff = _compute_stat_effectiveness(endurance)
    return _BASE_HP + int(endurance_eff * _BASE_HP)


def _compute_healing_power(stats):
    wisdom = stats[CharacterParams.WISDOM]
    wisdom_eff = _compute_stat_effectiveness(wisdom)

    intelligence = stats[CharacterParams.INTELLIGENCE]
    intelligence_eff = _compute_stat_effectiveness(intelligence)

    return _BASE_HEALING + int(wisdom_eff * _BASE_HEALING // 2) + int(intelligence_eff * _BASE_HEALING // 2)


def _compute_attack_power(stats):
    strength = stats[CharacterParams.STRENGTH]
    strength_eff = _compute_stat_effectiveness(strength)

    dexterity = stats[CharacterParams.DEXTERITY]
    dexterity_eff = _compute_stat_effectiveness(dexterity)

    return _BASE_ATTACK + int(dexterity_eff * _BASE_ATTACK // 2) + int(strength_eff * _BASE_ATTACK // 2)


class TestGame:
    def __init__(self, message):
        self.monster = message

    def play(self, stats):
        print("Your enemy is: {}".format(self.monster))

        attack = _compute_attack_power(stats)
        healing = _compute_healing_power(stats)
        initial_hp = _compute_initial_hp(stats)
        hp = initial_hp

        beast_attack = 40
        beast_hp = random.randint(70, 100)
        while beast_hp > 0 and hp > 0:
            print("HP: ", hp)
            print("Enemy HP: ", beast_hp)
            print("1: attack")
            print("2: heal yourself")
            action = input(">> ").strip()
            if action == "1":
                print("You attack!")
                beast_hp -= attack
            elif action == "2":
                print("You heal yourself!")
                print(initial_hp)
                print(healing)
                print(hp + healing)
                hp = min(initial_hp, hp + healing)
                print()
                print(hp)
            else:
                print("Unknown command")
                continue

            print("The beast attacks you!")
            hp -= beast_attack

        return hp > 0
