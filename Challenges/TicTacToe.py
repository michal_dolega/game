from collections import namedtuple
from Utils import validate_number
import random


class TokenType:
    def __init__(self, circle, cross):
        self.Circle = circle
        self.Cross = cross


class TicTacToe:

    def __init__(self, circle="O", cross="X"):
        self.board = [[" " for _ in range(3)] for _ in range(3)]
        self.gameToken = TokenType(circle=circle, cross=cross)
        self.player_token, self.ai_token = self.gameToken.Circle, self.gameToken.Cross
        self.movements_dict = {
            0: self.player_move,
            1: self.opponent_move
        }

    def __str__(self):
        result = ""
        for row in self.board:
            result += ("|".join(row) + "\n")
        return result

    def player_move(self):
        print("Player move")
        x = int(input("Give a row"))
        y = int(input("Give a col"))
        while not self.is_move_legal(x, y):
            print("Your movement is not legal. Choose coordinates once again")
            x = int(input("Give a row"))
            y = int(input("Give a col"))
        self.board[x][y] = self.player_token
    
    def opponent_move(self):
        print("Opponent move")
        x = random.randint(0, 2)
        y = random.randint(0, 2)
        while not self.is_move_legal(x, y):
            x = random.randint(0, 2)
            y = random.randint(0, 2)
        self.board[x][y] = self.ai_token

    def is_move_legal(self, x, y):
            return 0 <= x <= 2 and 0 <= y <= 2 and self.board[x][y] == " "

    def play(self, stats):
        movements_queue = ([0, 1] * 5)[:-1]
        is_ended = False
        winner = None
        for movement in movements_queue:
            self.play_turn(movement)
            is_ended, winner = self.check_result_and_winner()
            if is_ended:
                break
            print(self)
        if is_ended and winner is not None:
            print("The winner is: {0}".format(winner))
            print(self)
            if winner == self.player_token:
                return True
            else:
                return False
        else:
            print("The game finished with a tie")
            print(self)
            return False

    def play_turn(self, movement_id):
        self.movements_dict[movement_id]()

    def empty_fields(self):
        for row in self.board:
            for field in row:
                if field == " ":
                    return True
        return False

    def check_result_and_winner(self):
        return self.check_win_conditions()

    def check_win_conditions(self):
        for i in self.win_conditions_functions():
            #This magic number denotes piece of result tuple which decide who is winner || ex result = tuple(True, "O") || otherwise code won't recognize victory
            if i()[0]:
                return i()
        return False, None

    def check_crosses(self):
        if self.board[0][0] == self.board[1][1] == self.board[2][2] and self.board[0][0] != " ":
            return True, self.board[1][1]
        elif self.board[0][2] == self.board[1][1] == self.board[2][0] and self.board[0][2] != " ":
            return True, self.board[1][1]
        else:
            return False, None

    def check_columns(self):
        for i in range(3):
            if self.board[0][i] == self.board[1][i] == self.board[2][i] and self.board[0][i] != " ":
                return True, self.board[0][i]
        return False, None

    def check_rows(self):
        for row in self.board:
            if row[0] == row[1] == row[2] and row[0] != " ":
                return True, row[0]
        return False, None

    def win_conditions_functions(self):
        return [
            lambda: self.check_crosses(),
            lambda: self.check_columns(),
            lambda: self.check_rows()
        ]
