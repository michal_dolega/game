import random
from Utils import validate_number


class GuessNumber:
    def __init__(self):
        self.answer = random.randint(0, 10)
        self.winner = None

    def play(self, stats):
        while True:
            curr_answer = self.player_move()
            if self.check_answer(curr_answer):
                self.winner = "Player"
                break
            curr_answer = self.opponent_move()
            if self.check_answer(curr_answer):
                self.winner = "Opponent"
                break
        if self.winner == "Player":
            print("Congratulations! The correct number was: {}".format(self.answer))
            return True
        else:
            print("You lost! The correct number was: {}".format(self.answer))
            return False

    def check_answer(self, answer):
        return answer == self.answer

    def player_move(self):
        chosen_number = input("Provide number from 0 to 10\n")
        while not validate_number(chosen_number,0,10):
            chosen_number = input("Error. Provide number from 0 to 10\n")
        return int(chosen_number)

    def opponent_move(self):
        temp_val = random.randint(0, 10)
        print("Computer chose: {}".format(temp_val))
        return temp_val
