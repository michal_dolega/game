from pickle import dumps, loads

_SAVE_DESTINATION = "saved_game.bin"


def save_game(game_state):
    serialized = dumps(game_state)

    saved = open(_SAVE_DESTINATION, mode="wb+")

    saved.write(serialized)
    saved.flush()
    saved.close()


def load_game():
    saved = open(_SAVE_DESTINATION, mode="rb")

    serialized = saved.read()
    saved.close()
    return loads(serialized)
