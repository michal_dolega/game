from Challenges.TestGame import TestGame
from Challenges.GuessNumber import GuessNumber
from Challenges.TicTacToe import TicTacToe
from Location import Location
from Game import Game
import random


class GameFactory:

    def __init__(self, locations, monsters, levels):
        self.locations_list = locations
        self.monsters = monsters
        self.levels_number = levels
        self.game_facctories = [
            TicTacToe, 
            GuessNumber, 
            lambda: TestGame(random.choice(self.monsters))
            ]        

    def generate_game_state(self):
        levels_dict = {}
        for i in range(self.levels_number):
            locations_list = []
            for location in self.locations_list:
                locations_list.append(self.generate_location(location))
            levels_dict[i] = locations_list
        return levels_dict

    def generate_location(self, location_name):
        return Location(location_name, random.choice(self.game_facctories)())