from Challenges import TicTacToe, GuessNumber

LOCATIONS = ["Tower of London", "Magic Forest", "Hidden Cave", "Prince's Castle", "Shoemaker Workshop"]
LEVELS = ["Day1", "Day2", "Day3"]
MONSTERS = ["Ogre", "Rat", "Dragon", "Bull", "Dog", "Bear"]
